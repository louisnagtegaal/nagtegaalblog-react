import React from 'react';
import axios from 'axios';
import HomeButton from './components/HomeButton';
import BlogContent from './components/BlogContent';
import BlogFragment from './components/BlogFragment';
import NavItem from './components/NavItem';

// CSS
import './css/index.css';
import './css/App.css';

class App extends React.Component {
    // Constructor with default values.
    constructor(props) {
        super(props);
        this.state = {
            nodes: [],
            currentnode: null,
            curWidth: 300,
            curHeight: 300,
        }

        // We need to bind 'this' to make sure this refers to our app-object in the functions
        // see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this
        this.handleChildClicked = this.handleChildClicked.bind(this);
        this.handleHomeClick = this.handleHomeClick.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);

        document.title = process.env.REACT_APP_TITLE;
    }

    // Initiate object props and stae.
    componentWillMount() {
        let self = this; //Once inside the axios request, this will not be the same this.
        const baseurl = process.env.REACT_APP_BASE_URL;
        axios.get(baseurl + "jsonapi/node/blog?fields[node--blog]=title,created&sort=created")
            .then(function (response) {
                let nodes = response.data.data;
                self.setState({
                    nodes: nodes
                })
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    // Lifecycle functions for window resizing.
    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    // Remove event listener if component is destroyed.
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    // Function to set the windows dimensions in the state.
    updateWindowDimensions() {
        this.setState({
            curWidth: (window.innerWidth),
            curHeight: (window.innerHeight)
        });
    }

    // Handle click of the child navigation link.
    handleChildClicked(id) {
        this.setState({currentnode: id});
    }

    // Handle click of the home button.
    handleHomeClick() {
        document.title = process.env.REACT_APP_TITLE;
        this.setState({currentnode: null});
    }

    // Render the component.
    render() {
        let frag = false;
        let blog = false;
        if(this.state.currentnode === null) {
            frag = (
                <BlogFragment ref="blogFragment"
                              id={this.state.currentnode}
                              mWidth={this.state.curWidth}
                              mHeight={this.state.curHeight}
                />);
        } else{
            blog = (
                <BlogContent id={this.state.currentnode}/>
            );
        }
        return (
            <div id="content" className={"main-content" + (this.state.currentnode !== null ? ' hascontent' : '')}>
                { frag }
                <nav className={"navigation" + (this.state.currentnode !== null ? '  active' : '')}>
                    <HomeButton
                        homeClicked={this.handleHomeClick}
                        title="Home"
                        active={this.state.currentnode === null}
                        mWidth={this.state.curWidth}
                        mHeight={this.state.curHeight}
                    />
                    {
                        this.state.nodes.map((n) => {
                            return(
                                <NavItem
                                    currentNode={n}
                                    key={n.id}
                                    childClicked={this.handleChildClicked}
                                    active={this.state.currentnode === n.id}
                                    mWidth={this.state.curWidth}
                                    mHeight={this.state.curHeight}
                                />
                            );
                            }
                        )}
                </nav>
                { blog }
            </div>
        )
    }
}

export default App;
