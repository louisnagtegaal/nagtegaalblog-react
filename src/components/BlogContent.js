import React from 'react';
import axios from 'axios';
import BlogImage from './BlogImage';

class BlogContent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            blog: false,
            title: false,
            body: false
        };
    }

    componentDidUpdate () {
            window.scrollTo(0, 0);
    }

    render() {
        if(this.props.id == null){
            return null;
        }
        else {
            let currentBlog = false;
            let self = this; //Once inside the axios request, this will not be the same this.
            if(this.state.blog !==  this.props.id) {
                const baseurl = process.env.REACT_APP_BASE_URL;
                axios.get(baseurl + "jsonapi/node/blog/" + this.props.id)
                    .then(function (response) {
                            currentBlog = response.data.data;
                            let altText = "";
                            if(currentBlog.relationships.field_image.data !== null){
                                altText = currentBlog.relationships.field_image.data.meta.alt;
                            }
                            self.setState({
                                blog: currentBlog.id,
                                title: currentBlog.attributes.title,
                                body: currentBlog.attributes.body.value,
                                imgReq: currentBlog.relationships.field_image.links.related,
                                imgAlt: altText
                            });
                            document.title = process.env.REACT_APP_TITLE + ': ' + currentBlog.attributes.title;
                        }
                    );
            }
            let blogImage = false;
            // Because in the backend the alt is required if an image is added we can check this way if the image exists
            if(this.state.imgAlt !== "") {
                blogImage = <BlogImage url={this.state.imgReq} alt={this.state.imgAlt}/>;
            }
            return (
                <div className="blog-content">
                    <h2>{this.state.title}</h2>
                    { blogImage }
                    <div className="body" dangerouslySetInnerHTML={{__html: this.state.body}}></div>
                </div>);
        }
    }
}

export default BlogContent;