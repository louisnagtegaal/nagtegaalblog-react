// Node Component

import React from 'react';

class NavItem extends React.Component {

    constructor(props) {
        super(props);
        this.handleNavClick = this.handleNavClick.bind(this);
        this.oneMonth = 30*24*60*60*1000;
        this.oneDay = 24*60*60*1000;
    }

    handleNavClick(e) {
        this.props.childClicked(e.target.id);
        e.preventDefault();
    }

    calcX(){
        let dateAttraction = this.calcAttraction();
        let maxWidth = this.props.mWidth;
        let cw = (maxWidth / 2) -180;
        let cx = cw + Math.floor(Math.cos(this.props.currentNode.attributes.created) * cw);
        cx > cw ? cx -= dateAttraction: cx += dateAttraction;
        return cx;

    }

    calcY(){
        let dateAttraction = this.calcAttraction();
        let maxHeight = this.props.mHeight;
        let ch = (maxHeight / 2);
        let cy = ch + Math.floor(Math.sin(this.props.currentNode.attributes.created) * ch);
        cy > ch ? cy -= dateAttraction: cy += dateAttraction;
        return cy;

    }

    calcAttraction(){
        let created = this.props.currentNode.attributes.created;
        return Math.floor((Date.now() - created) / (this.oneMonth * 2));
    }

    render() {
        // We get the current full width and height from parent.
        // Make sure we scale them and substract width of the nav-items
        let setZindex = Math.floor(this.props.currentNode.attributes.created / 1000);
        const divStyle = {
            left: this.calcX(),
            top:  this.calcY(),
            zIndex: setZindex,
        };
        return (
            <div className="blog-link" style={divStyle}>
                <button
                    className={"link-button " + (this.props.active ? 'active' : '')}
                    onClick={this.handleNavClick}
                    id={this.props.currentNode.id}>{this.props.currentNode.attributes.title}</button>
            </div>
        )
    }
}

export default NavItem;