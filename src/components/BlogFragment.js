import React from 'react';
import axios from 'axios';

class BlogFragment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: false,
            fragment: false,
        };
    }

    updateFragment() {
        let self = this; //Once inside the axios request, this will not be the same this.
        const baseurl = process.env.REACT_APP_BASE_URL;
        axios.get(baseurl + "fragments_api/fragment?_format=json&noCache=" + Date.now())
            .then(function (response) {
                    let title= response.data.title;
                    let fragment = response.data.fragment;
                    self.setState({
                        fragment: fragment,
                        title: title,
                    });
                }
            );
    }

    componentDidMount() {
        this.intervalID = setInterval(
            () => this.updateFragment(),
            6000
        );
    }

    componentWillUnmount() {
        clearInterval(this.intervalID);
    }

    render() {
        // Do nothing if we are on a blog page.
        if (this.props.id != null) {
            return null;
        }
        let self = this; //Once inside the axios request, this will not be the same this.
        if (!this.state.fragment) {
            const baseurl = process.env.REACT_APP_BASE_URL;
            axios.get(baseurl + "fragments_api/fragment?_format=json")
                .then(function (response) {
                        let title= response.data.title;
                        let fragment = response.data.fragment;
                        self.setState({
                            fragment: fragment,
                            title: title,
                        });
                    }
                );
        }
        // Min and max fontsize depend on screen width, set min defaults for mobile
        let minFontSize = this.props.mWidth / 64;
        let maxFontSize = this.props.mWidth/ 32;
        if(minFontSize < 12){
            minFontSize = 12;
        }
        if(maxFontSize < 20){
            minFontSize = 20;
        }
        // @TODO calcultate number of fonts and colors from stylesheet is possible
        let numFonts = process.env.REACT_APP_NUM_FONTS;
        let numColors = process.env.REACT_APP_NUM_COLORS;
        let cw = (this.props.mWidth / 2);
        let ch = (this.props.mHeight / 2);
        let color = 'color-' + (1 + Math.floor((Math.random() * numColors)));
        let font = 'font-' + (1 + Math.floor((Math.random() * numFonts)));
        let fontSize = minFontSize + Math.floor((Math.random() * maxFontSize));
        let contentClass = "content " + font + ' ' + color;
        let divStyle = {
            left: cw + ((Math.random() * cw) - cw),
            top: ch + ((Math.random() * ch) - ch),
            //zIndex: 20,
            fontSize: fontSize,
        };
        return (
            <div className="blog-fragment" style={divStyle}>
                <div className={contentClass} dangerouslySetInnerHTML={{__html: this.state.fragment}}></div>
                <h3>{this.state.title}</h3>
            </div>);
    }
}

export default BlogFragment;