import React from 'react';
import axios from 'axios';

class BlogImage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            url: false,
            imgUrl: "",
            alt: ""
        };
    }

    render() {
        if(this.props.url == null){
            return null;
        }
        else {
            let self = this; //Once inside the axios request, this will not be the same this.
            if(this.state.url !==  this.props.url) {
                console.log(self.props.url.href);
                axios.get(self.props.url.href)
                    .then(function (response) {
                        let currentImg = response.data.data;
                        console.log(currentImg);
                        if(typeof currentImg !== 'undefined' && typeof currentImg.attributes !== 'undefined') {
                            self.setState({
                                url: self.props.url,
                                imgUrl: process.env.REACT_APP_BASE_URL + currentImg.attributes.uri.url,
                                alt: self.props.alt
                            });
                        }
                    });
            }
            return (
                <div className="blog-image">
                    {this.state.imgUrl}
                   <img src={this.state.imgUrl} width="70%" alt={this.state.alt}/>
                </div>);
        }
    }
}

export default BlogImage;