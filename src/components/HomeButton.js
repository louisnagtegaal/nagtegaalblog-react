import React from 'react';

class HomeButton extends React.Component {
    constructor(props) {
        super(props);
        this.handleHomeClick = this.handleHomeClick.bind(this);
    }

    handleHomeClick(e) {
        document.title = process.env.REACT_APP_TITLE;
        this.props.homeClicked();
    }

    render() {

        const homeDate = new Date('1964-10-06T03:30:00');
        // We get the current full width and height from parent.
        // Make sure we scale them and substract width of the nav-items
        let cw = (this.props.mWidth / 2) - 180;
        let ch = (this.props.mHeight / 2) - 180;
        let divStyle = {
            left: cw + Math.floor(Math.sin(homeDate) * cw),
            top:  ch + Math.floor(Math.cos(homeDate) * ch),
            zIndex: Math.abs(homeDate / 1000),
        };
        return (
            <div className="home-link" style={divStyle}>
                <button
                    className={"link-button " + (this.props.active ? 'active' : '')}
                    onClick={this.handleHomeClick}
                    id='home'>{this.props.title}</button>
            </div>
        )
    }
}

export default HomeButton;